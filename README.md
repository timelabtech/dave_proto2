# prototype

This project is a basic prototype using Flask, jQuery, and pygal for the graphs.
See [demonstration video](https://drive.google.com/file/d/0B9GpD3rtbI_6LXZkTVk2NmtEU2s/view?usp=sharing).

Author [crystal95](https://github.com/crystal95/prototype)

To view the functionality

1. Setup a virtual python environment with all the required packages installed
2. Run the app.py using "python app.py" in the terminal
3. Go to http://localhost:5000 in the browser